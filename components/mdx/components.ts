import Anchor from 'components/mdx/Anchor';
import Image from 'components/mdx/Image';
import Pre from 'components/mdx/Pre';
import RUACodepen from 'components/RUA/RUACodepen';
import RUACodeSandbox from 'components/RUA/RUACodeSandbox';
import RUASandpack from 'components/RUA/RUASandpack';
import Tab from 'components/RUA/tab';
import TabItem from 'components/RUA/tab/TabItem';

const components = {
  RUASandpack,
  a: Anchor,
  pre: Pre,
  Image,
  Tab,
  TabItem,
  RUACodeSandbox,
  RUACodepen,
};

export default components;
